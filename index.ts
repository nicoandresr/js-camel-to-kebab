const camelCaseRegex = /([A-Za-z](?=[A-Z\d])|\d(?=[A-Z]))/g;

export default t => t.replace(camelCaseRegex, '$&-').toLowerCase();
