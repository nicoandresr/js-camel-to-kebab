# js-camel-to-kebab

Utility for convert camel case to kebab case.

## Install

    npm install js-camel-to-kebab

## Synopsis

Convert camel case string to kebab case.

```javascript
var test = require('tape');

// you must import like
// import camelToKebab from 'js-camel-to-kebab';
var camelToKebab = require('./umd/').default;

test('Convert to kebab case from camel case', function (t) {
  t.plan(1);
  t.equal(camelToKebab('HelloWorld'), "hello-world");
});
```
Also if the string have kebab case then this don't is coverted
```javascript
test('Don`t convert already kebab case', function (t) {
  t.plan(1);
  t.equal(camelToKebab('this-string-already-haveKebabCase'), 'this-string-already-have-kebab-case');
});
```
